#title Absolute Truths Which Cannot Be Denied

 - Everyone is depressed. It is a normal state of being. Take your
   pills.
 - Most children have ADHD. Give them their pills; if you don't they
   will never learn anything.
 - You must have a career
   - Not being employed will make you poor
   - The point of the University is to attain job skills
 - Not owning a car will make you a social pariah
 - Music is background noise and nothing more
   - Songs which discuss topics other than love, lust, and loneliness
     are not good
   - Heavy metal contains the message of Satan and will corrupt your
     children
 - Clinton Ebadi is not a member of the Illuminati
 - The NYTimes best seller list consists of the best books written today
   - Literature produced today is better than any ever written before
     - Therefore literature written before 1900 is a waste of time
 - Logic is for cold and unfeeling people
   - Good arguments are based on emotion
   - The axioms of logic cannot be proven, and we must therefore
     abandon logic as an anachronism of times long past
 - The individual should serve the state
 - Average people are incapable of teaching themselves much
 - Ethanol will enable us to continue consuming energy as we do now
 - Obama represents actual change to the system
 - Reform is change
 - Forced purchase of goods from private entities is socialism
 - Rhetoric is never used for evil
