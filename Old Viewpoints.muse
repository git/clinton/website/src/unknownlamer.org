#title The Weak Must Die

Intermediate forms of thought which have been refined.

* Misc

** Copyright Is Bad for Society

Copyright is a tool used to placate publishers who feel that they will
make no money if things can be freely copied. Publishers, however,
contribute **nothing** of worth to our culture; they are mere middlemen
who print the creative work of others, and so their pleas should be
ignored.

A short copyright term is acceptable, and worked in most of the world
for a few hundred years. As it stands now we have perpetual copyrights
(as in the Old World), and the cultural stagnation that affected
Europe then is now occurring today in most of the world. There are
many books published between 1917 and a few years ago that I would
love to read, but am unable to because they have not been printed (for
older books often in as long as 40 or 50 years). The albums of a few
bands I like are out of print now and I will be long dead before I get
a chance to purchase them (*if* copyright is not extended again, which
experience tells me will happen soon) because the record labels have
no interest in returning the masters to the band!

What point is there to allowing copyright to exist on works which are
not being published? If their terms had expired there is a chance that
they would be being published by public domain publishing houses who
subsist on smaller margins. This would [[http://www.ippr.org/publicationsandreports/publication.asp?id=482][create real economic value]], and
more importantly great **social** value. Allowing art to rot is a
disrespect to human creativity and an immeasurable loss for all future
humans.

I predict that in two or three hundred years there will be nearly no
record of any literature or art produced in the twentieth century. As
it stands today we have lost most of it with the exception of a few
trashy works which have become popular to the masses.
