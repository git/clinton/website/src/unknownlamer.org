#title The Printed Word Is Stronger Than Nuclear Arms

I enjoy classical literature and works of philosophy and politics with
a side of cyberpunk novels for when my brain is tired. When I was in
High School I read technical books for fun, but now I tend to find
most of them useless (thank you Internet) excepting a few really well
written ones (*L.i.s.p*, *TAOCP*, ...).

I spend most of my time reading. A full list of things I have read
would be impossible to compile, but here I am collecting links and
small summaries of things I have read and find interesting enough to
mention, but not always recommend, to others.

* Authors

** William Blake

His poetry is the result of spending too much time etching copper
plates and breathing the fumes. Quite wonderful indeed.

** Kahlil Gibran

Kahlil Gibran is fairly interesting; his earlier works do not agree
with my æsthetic sense (blah blah), but *The Madman* onward are all
rather nice. So far I've read *A Tear and a Smile* (not so good
excepting the last poem), *The Madman*, *The Prophet* (both excellent),
and *Sand and Foam* (an interesting little book of aphorisms). A few of
his works are [[http://leb.net/~mira/][online]], but I recommend scouting used book stores for
old hardcover editions. The (late 90s onward at least) *hardcover*
versions from *Alfred A. Knopf* are in fact permabound paperbacks with a
hardcasing, and are of seriously inferior quality to the editions from
the 50s and 60s (and cost quite a bit more, naturally).

* Fiction

** General

*** Luke Rhinehardt - The Dice Man

<quote>
And it's his illusions about what
constitutes the real world which are
inhibiting him...
His reality, his reason, his society
...these are what must be destroyed
</quote>

A quotation from one of my [[http://en.wikipedia.org/wiki/Slaughter_of_the_Soul][favorite metal songs]] inspired me to grab
this book; at worst it would be a waste of time. Much reward was found
in this random stab in the dark. The book is framed as an
autobiography of the author as a psychoanalyst, and his progression
through life as a Dice Man after deciding to live his life through
random chance.

The style, plot, and content are equally neurotic; part comedy, part
attack on psychoanalysis, and part deep philosophy. It was often
difficult to put down, and was read in under a week of spare time.

** Philosophical

** Sci-Fi

*** Neal Stephenson

**** Cryptonomicon

I read *Cryptonomicon* when it was new, and at the time I thought it was
good. It could have lost a hundred or so pages without detracting from
the plot, but it was easy reading and didn't take very long to
finish. The story was enganging, and the continual switching between
the 1940s and present day slowly unravelled the tale in a nice way.

I'd still have to recommend *Snow Crash* if one wished to read only one
Stephenson novel.

**** Snow Crash

As one must read the *Bible* to understand English literature, so one
must read *Snow Crash* today to be a nerd. In the realm of modern pop
fiction this is one of the better books I've read; it was devoured in
a mere four nights. Neal Stepheson may not be Milton, but he does come
up with enganging tales. *Snow Crash* has a nice undertone of (quite
accurate) political and social commentary that makes it worth reading
as more than mere cyberpunk fiction.

* Non-Fiction

** Education

***  John Taylor Gatto - [[http://www.johntaylorgatto.com/underground/toc1.htm][Underground History of American Education]]

Contained within this book (available online for free, but the printed
copy sits wonderfully on a shelf) is a detailed and seemingly well
researched history of American Education with a particular focus on
the transformation that has occured before our eyes in the last
century. I am unsure if Gatto is entirely correct and not exaggerating
anything; I have failed to find any negative criticisms, but it is not
clear to me if that is because he is entirely correct or if no one
cares enough to write a counterargument. I am in the process of
tracking down as many of his sources as possible (a good number of
them are out of print and not in the public domain yet), and will make
an attempt to verify his argument over the course of the next year
(that being 2007).

If he is correct then every one of us has had the first eighteen years
of our lives stolen from us, and we have collectively suffered massive
intellectual damage. My intuitions tell me he is correct (which is why
I am driven to verify; I cannot trust myself because I *want* to
believe) for my individuality and intelligence were nearly stolen from
me. The only reason I survived relatively unscathed is because I
became completely socially withdrawn for the last half of elementary
school until late in high school due to the abuse I received at the
hands of my peers creating a deep fear of social interaction in
me. The downside is that I had the confidence crushed from my soul,
but now that I have begun to regain it (the good that bicycling
enabling me to stand straight and gradual realization of my own worth
as a human have done) I would never trade the ability to think freely
for the social skills I lack.

** Philosophy

*** Chinese

**** Tao Te Ching

**** Confucianism

***** The Analects

*** Marcus Aurelius - Meditations

I enjoyed reading this collection of meditations on Stoic
philosophy. It is a fairly quick read; I read each of the twelve books
before sleeping over the course of two weeks. Toward the end of the
collection things get a bit topically repetetive (e.g. acting
according to the nature of man is reflected upon over and over), but
each repetition looks at the topic in a slightly different light. A
number of passages I found quite inspiring, and scratched them down in
my notebook to ponder further.

*** Søren Kierkegaard - The Sickness Unto Death

I purchased this when I was looking through books at a store after
being unable to find the book I really wanted, and I must say that it
was better for me to have found this one. 

Contained within is a beautiful analysis of despair in the context of
Christianity (really theism in general). Even if the argument offends,
the presentation cannot. The dialectical nature of despair is
reflected in every aspect of the work, and the method of presentation
forces reflection. 

** Politics

*** Thomas More - Utopia

I read most of Utopia in high school with the TI-89 ebook reader, but
the way the book was split up made it a bit difficult to grasp the
overall structure. I found a copy at a used book store one day, and so
I read it again, and found it much more comprehensible. It is a quick
read, and decent piece of literature. The interesting social system
espoused resembles resembles state communism (even if perhaps as a
negative ideal), but with an strange blend of 14th century European
social customs.

** Religion

*** [[William James - The Varieties of Religious Experience]]


** Technical

*** C J Date - Database in Depth

This was a complete waste of time. The author rants on for 180 pages
and presents the information in a disorderly and shallow manner. It
could be rewritten in about fifty pages and contain the same amount of
information if it were organized properly and the off topic commentary
were minimized.

*** Gregor Kiczales - The Art of the Metaobject Protocol

AMOP is useful as a reference to the CLOS MOP (although less so with
the online MOP spec), but the true value of the book lies in the first
half of the book. It presents the design of the CLOS MOP through a
series of revisions that fix limitations of earlier implementations
and gradually work toward a generic and well designed MOP for
CLOS. Through that process one is made more aware of a few general
object protocol design skills, and gains insight into how to cleanly
make mapping decisions customizable.


* Reports

** [[http://nces.ed.gov/pubs2006/2006483.pdf][2003 National Assesement of Adult Literacy]]

A depressing view of American literacy rates. Literacy skills
decreased across almost every population segment in the US between
1993 and 2003; a mere 31% of college graduates are considered
proficient in quantitative literacy (defined as being able to do
things as terribly complicated as comparing two editorials).


* Books That I Cannot Find

If you know anyone who has copies I'd appreciate an email. I'm willing
to buy books for a reasonable cost, and for ones that are more than 25
years old and out of print I am not opposed to *piracy* (no one is
making money from them, and I feel that long copyrights are unethical
and therefore feel no pangs of moral guilt).

 - *Crystallizing Public Opinion* by Edward Bernays
   - A supposed classic in the field of public relations. Curiosity
     demands that I read the writings of the father of the field to
     better understand the way the international media works.
   - Another example of out of print books clearly having a market,
     but no publisher due to copyright (used copies go for nearly a
     thousand dollars in poor condition and hit five thousand or so
     for ones in good shape).

* Essays

** Computing

*** Design

**** [[http://deadhobosociety.com/index.php/Essays/ESSAY12][Confucianism and Technical Standards]]
