#title Behold the Mighty Kilt

* Lust

Once upon a time I went to see [[http://eluveitie.ch/][Eluveitie]] on a little tour called
[[http://www.paganfest.net/info.php?lang=usa][Paganfest]]. The bassist and piper of Eluveitie were wearing kilts and
they looked mighty fine and metal; thus began my lust for a kilt of my
very own.

* Goodbye Money

A few months later I decided to purchase a [[http://www.utilikilts.com/index.php?page_id=30][Workman's]] [[http://utilikilts.com][Utilikilt]] as it
appeared to be the most practical kilt for every day wearing. After
much fretting about sizing and ordering such an expensive piece of
nonreturnable clothing I finally ordered one in August 2008 and had it
in my hands the first week of September. Luckily, it fit.

[[img/photos/kilt/package.jpg][One day an unassuming package arrived in the mail]]


[[img/photos/kilt/unpacked-folded.jpg][Such a lovely unbifurcated garment]]

The kilt is made of very heavy canvas (it weighs about four pounds)
and was of quite good construction. I'd say it was worth the pretty
penny I spent for it.

[[img/photos/kilt/unpacked-spread.jpg][Why it appears to be a skirt of some sort]]

After unpacking and gazing upon the glory of the kilt I donned it upon
my waist and decided that I should never wear pants again. Everything
they say is true: the pockets really can fit a six pack of beer, it is
quite comfortable, it doesn't look dumb (ok, perhaps that is
subjective), and it *is* handy to have my tools on my kilt when working
on my [[My Bicycle][bicycle]].

* STEEL

[[img/photos/kilt/eluveitie.jpg][I am quite the dashing young lad]]

I first wore the kilt in public about an hour after getting it, and I
must say that I find it amusing that people would fret about wearing a
kilt outside of their house. I now have a new problem: I want more
kilts so that I no longer have to wear pants.
