I travel mostly by bicycle, and have to plot routes out for myself
often. I think some may be useful to people in the Baltimore area of
Maryland.

* My Routes

** [[http://www.bikely.com/listpaths/by/clinton][Bikely Routes]]
** [[Cary to Greensboro Bicycle Route]]

* Maryland/DC Routes

** [[http://bikewashington.org/routes/all.htm][Bikewashington]]

A nice collection of routes for travelling around Maryland with full
cues and maps.
