#title Dancing Robot Death Machines

* Useful Free Software for Android

We all like Free Software here right?

As per the request of my dearest friend emacsen I bring you a list of
Free Software I use on Android. Nowadays, I'd point folks toward
[[http://f-droid.org][F-Droid]], the **excellent** market for Free Software applications. During
2012 it made huge steps forward, and now contains software for
basically every common task (even a [[http://f-droid.org/repository/browse/?fdfilter=nyan&fdid=com.powerje.nyan][Nyan Cat live background]], if
you're into that sort of thing).


** Network

 - [[http://code.google.com/p/k9mail/][K-9 Mail]] Replacement for the builtin Email app that has a few more
   features that make it much nicer to use.
 - [[http://code.google.com/p/connectbot/][ConnectBot]] Very useful ssh/telnet and local shell terminal
   emulator.

** Utilities

 - [[http://www.openintents.org/en/download][OpenIntents Applications]] A basic notepad, todo list, file manager,
   news reader, countdown timer, flashlight, and encryption thing. You
   can put shortcuts to notes on the homescreen which is quite handy.

** Games

 - [[market://search?q=pname:com.netthreads.android.noiz2][Noiz2]] (market link). Pretty fun. [[http://ccgi.arutherford.plus.com/blog/wordpress/projects/][netthreads]] has more projects that
   might be useful too.
 - [[http://m.smsbox.ch/pro/frozenbubble][Frozen Bubble]] Productivity, farewell.
 - [[http://code.google.com/p/doom-for-android/][Doom for Android]] This is actually playable on a g1.

** Development

 - [[http://www.gnu.org/software/kawa/][Kawa Scheme]] can indeed [[http://per.bothner.com/blog/2009/AndroidHelloScheme/][be used to write Android applications]]
   (mildly out of date, but useful nonetheless). I swear, I've done
   it, and I'll do it again.
 - [[http://www.emacswiki.org/emacs/EmacsOnAndroid][Emacs on Android]]. Why not?
